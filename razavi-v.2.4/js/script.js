//_______________________________________________________________________________________
// on scroll effects
function openTab (evt, tabName, sectionName = null) {
    var i, tabcontent, tablinks;
    tabcontent = $( sectionName + " .tab_detail");
    $.each(tabcontent, function() {
        tabcontent.css('display', 'none');
    } )
    tablinks = $( sectionName + " .tab_link");
    $.each(tablinks, function() {
        tablinks.removeClass('active') ;
    } )
    // Show the current tab, and add an "active" class to the button that opened the tab
    $( sectionName + " " + tabName).css('display' , 'block');
    evt.currentTarget.className += " active";
}


//_______________________________________________________________________________________
// animation on scroll 
var topOfWindow, bottomOfWindow, prevScrollpos;
function calculatePositions () {
    topOfWindow = $(window).scrollTop();
    bottomOfWindow = $(window).height() + $(window).scrollTop();
}

var animation_elements = $(".animate__animated");

// animate elements when in view 
function checkIfInView() {

    var windowHeight = $(window).height();
    var windowTopPosition = $(window).scrollTop();
    var windowBottomPosition = (windowTopPosition + windowHeight);
    
    
    $.each(animation_elements, function() {
        var thisElement = $(this);
        var elementHeight = thisElement.outerHeight();
        var elementTopPosition = thisElement.offset().top;
        var elementBottomPosition = (elementTopPosition + elementHeight);
        
        //check to see if this current container is within viewport
        if ((elementBottomPosition >= windowTopPosition) &&
        (elementTopPosition + 100 <= windowBottomPosition)) {
            thisElement.addClass('in_view');
        } else {
            if ( !thisElement.hasClass('animate_once') ) 
            thisElement.removeClass('in_view');
        }
    });
}

//_______________________________________________________________________________________
// hide and show menu

prevScrollpos = window.pageYOffset;
function hideAndShowMenu () {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("mobile_navbar").style.top = "0";
    document.getElementById("nav_menu").style.top = "0";
    // $(navbar).addClass('sticky');

  } else {
    document.getElementById("mobile_navbar").style.top = "-60px";
    document.getElementById("nav_menu").style.top = "-58px";
    // $(navbar).removeClass('sticky');

  }
  prevScrollpos = currentScrollPos;
}

//_______________________________________________________________________________________
//sticky navbar after reaching heading

const navbar = document.getElementById("nav_menu");
const header = document.getElementById("header");
function showSticky() {
    var offsetBottom = header.offsetTop + header.offsetHeight;
    var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    if (scrollTop >= offsetBottom) {
        $(navbar).addClass('sticky');
    } else {
        $(navbar).removeClass('sticky');
    }
  }

//_______________________________________________________________________________________
// window.onscroll

function runOnScrollStuff() {
    calculatePositions();
    checkIfInView();
    showSticky();
    hideAndShowMenu();
    
}

$(window).scroll(runOnScrollStuff);

//_______________________________________________________________________________________
// open & close mobile menu

const menuBtn = $('#hamburger');
const menuOverlay = $('#menu_overlay');
const mobileMenu = $('#mobile_menu');
// const dropDownbuttons = $('.dropDownbutton');

let menuIsOpen = false;

// menuBtn.click();
menuBtn.click(toggleMenu);


function toggleMenu() {
    if (!menuIsOpen) {
        // open hamburger 
		mobileMenu.addClass('open');
		// show menu
		menuOverlay.fadeToggle(200);
		// menu is open
		menuIsOpen = true;
	} else {
        // close hamburger
		mobileMenu.removeClass('open');
		//hide menu
		menuOverlay.fadeToggle(200);
		// menu is closed
		menuIsOpen = false;
	}
}
menuOverlay.on('click', function(){
    menuOverlay.fadeToggle(200);   
	mobileMenu.removeClass('open');
	// menuBtn.removeClass('open');
	menuIsOpen = false;
});

//______________________________________________________________
//open & close dropdown menu

const dropDownbuttons = $('.dropdown_button');

dropDownbuttons.on('click' , function() {
    var thisBtn = $(this);
    var active = "active";
    // var btns = $("#mobileMenu .dropDownbutton");
    
	// if this btn is already active
	if (thisBtn.hasClass(active)) {
        thisBtn.next().slideUp();
        thisBtn.removeClass(active);
	}
	else {
		thisBtn.addClass(active);
		thisBtn.next().slideDown();
	}
})
//______________________________________________________________
//for loader hiding

window.onload = function() {
    document.getElementById("loader-wrapper").classList.add("loaded");
};

